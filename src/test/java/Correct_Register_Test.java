import config.Config;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.RegisterPage;

import static testdata.TestDataGenerator.generateRandomValidEmailAddress;

public class Correct_Register_Test extends Selenium_Base_Test {
    @Test
    public void shouldUserRegisterTest() {
        LoginPage loginPage = new LoginPage(driver);
        RegisterPage registerPage = loginPage.goToRegisterPage();
        registerPage.typeEmail(generateRandomValidEmailAddress());
        registerPage.typePassword(new Config().getApplicationPassword());
        registerPage.typeConfirmationPassword(new Config().getApplicationPassword());
        HomePage homePage = registerPage.submitRegister();

        Assert.assertTrue(homePage.generalElm.isDisplayed(), "Element GENERAL is not shown");
    }
}
