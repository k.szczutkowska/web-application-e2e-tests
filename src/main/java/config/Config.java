package config;

import java.io.InputStream;
import java.util.Properties;

public class Config {
    private final Properties properties;

    public Config() {
        properties = getProperties();
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            prop.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load properties file: " + e);
        }
        return prop;
    }

    public String getApplicationUrl() {
        return properties.getProperty("application.url");
    }

    public String getApplicationEmail() {
        return properties.getProperty("application.email");
    }

    public String getApplicationPassword() {
        return properties.getProperty("application.password");
    }

    public String getProcessesUrl() {
        return properties.getProperty("processes.url");
    }
    public String getCharacteristicsUrl() {
        return properties.getProperty("characteristics.url");
    }
}