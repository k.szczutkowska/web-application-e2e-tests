import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Menu_Navigation_Test extends Selenium_Base_Test {

    @Test
    public void menuTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationEmail())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessesUrl(new Config().getProcessesUrl())
                .assertProcessesHeaderIsShown()
                .goToCharacteristics()
                .assertCharacteristicsURL(new Config().getCharacteristicsUrl())
                .assertCharacteristicsHeaderIsShown()
                .goToDashboard()
                .assertDashboardUrl(new Config().getApplicationUrl())
                .assertDemoProjectIsShown();
    }
}
