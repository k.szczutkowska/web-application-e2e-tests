package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {
    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title h2")
    private WebElement pageHeader;

    public DashboardPage assertDashboardUrl(String dashboardUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), dashboardUrl);

        return this;
    }

    public DashboardPage assertDemoProjectIsShown() {
        Assert.assertTrue(pageHeader.isDisplayed());
        Assert.assertEquals(pageHeader.getText(), "DEMO PROJECT");

        return this;
    }
}
