import config.Config;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;

import static testdata.TestDataGenerator.generateRandomValidEmailAddress;

public class Incorrect_Register_Test extends Selenium_Base_Test {
    @Test
    public void shouldUserNotRegister_PasswordAndConfirmationPasswordDoNotMatch() {
        LoginPage loginPage = new LoginPage(driver);
        RegisterPage registerPage = loginPage.goToRegisterPage();
        registerPage.typeEmail(generateRandomValidEmailAddress());
        registerPage.typePassword(new Config().getApplicationPassword());
        registerPage.typeConfirmationPassword(new Config().getApplicationPassword() + "A");
        registerPage.submitRegisterFailure();
        registerPage.assertConfirmationPasswordErrorIsShown("The password and confirmation password do not match.");
    }

    @Test(dataProvider = "Invalid Passwords")
    public void shouldUserNotRegister_InvalidPassword(String password, String error) {
        LoginPage loginPage = new LoginPage(driver);
        RegisterPage registerPage = loginPage.goToRegisterPage();
        registerPage.typeEmail(generateRandomValidEmailAddress());
        registerPage.typePassword(password);
        registerPage.typeConfirmationPassword(password);
        registerPage.submitRegisterFailure();
        registerPage.assertInvalidPasswordErrorIsShown(error);
    }

    @DataProvider(name = "Invalid Passwords")
    public static Object[][] methodProvidingInvalidPasswords() {
        return new Object[][]{
                {"tE1!", "The Password must be at least 6 and at max 100 characters long."},
                {"test!12345test!12345test!12345test!12345test!12345test!12345test!12345test!12345test!12345test!12345A",
                        "The Password must be at least 6 and at max 100 characters long."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"TEST1!", "Passwords must have at least one lowercase ('a'-'z')."},
                {"testA!", "Passwords must have at least one digit ('0'-'9')."},
                {"Test12", "Passwords must have at least one non alphanumeric character."},
                {"", "The Password field is required."}
        };
    }
}
