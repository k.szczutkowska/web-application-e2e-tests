import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Create_Process_Test extends Selenium_Base_Test {

    @Test
    public void addProcessTest() {
        String processName = UUID.randomUUID().toString().substring(0, 10);
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationEmail())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .assertProcessExists(processName, "", "");
    }
}
