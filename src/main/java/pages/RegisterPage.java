package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class RegisterPage {

    protected WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement passwordConfirmTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement submitBtnRegister;

    @FindBy(css = ".validation-summary-errors")
    private WebElement passwordErrors;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement confirmPasswordError;

    public RegisterPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public RegisterPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public RegisterPage typeConfirmationPassword(String confirmationPassword) {
        passwordConfirmTxt.clear();
        passwordConfirmTxt.sendKeys(confirmationPassword);

        return this;
    }

    public RegisterPage submitRegisterFailure() {
        submitBtnRegister.click();

        return this;
    }

    public HomePage submitRegister() {
        submitBtnRegister.click();

        return new HomePage(driver);
    }

    public RegisterPage assertConfirmationPasswordErrorIsShown(String expectedError) {
        Assert.assertTrue(confirmPasswordError.isDisplayed());
        Assert.assertEquals(confirmPasswordError.getText(), expectedError);

        return this;
    }

    public RegisterPage assertInvalidPasswordErrorIsShown(String expectedError) {
        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors li"));

        Assert.assertTrue(passwordErrors.isDisplayed());
        Assert.assertNotNull(validationErrors.get(0));
        Assert.assertEquals(validationErrors.get(0).getText(), expectedError);

        return this;
    }
}
