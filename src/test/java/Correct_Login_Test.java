import config.Config;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;


public class Correct_Login_Test extends Selenium_Base_Test {

    @Test
    public void shouldUserLoginTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail(new Config().getApplicationEmail());
        loginPage.typePassword(new Config().getApplicationPassword());
        HomePage homePage = loginPage.submitLogin();

        Assert.assertTrue(homePage.generalElm.isDisplayed(), "Element GENERAL is not shown");
    }
}
