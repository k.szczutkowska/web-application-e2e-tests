package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateProcessPage extends HomePage {
    public CreateProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Name")
    WebElement processNameTxt;

    @FindBy(css = "input[type=submit]")
    WebElement submitCreateBtn;

    public CreateProcessPage typeName(String processName) {
        processNameTxt.clear();
        processNameTxt.sendKeys(processName);

        return this;
    }

    public ProcessesPage submitCreate() {
        submitCreateBtn.click();

        return new ProcessesPage(driver);
    }
}